"""
Vigenere.py
Encodes and decodes messages using the Vigenere cipher
Author: Richard Harrington
Created: 10/21/2013
"""

letters="ABCDEFGHIJKLMNOPQRSTUVWXYZ .1234567890"

def encode_msg(message, key):
    
    keys=[letters.index(c.upper()) for c in key]
    msg=[letters.index(c.upper()) for c in message]
    code=[]
    for i in range(len(msg)):
        code.append(letters[(msg[i]+keys[i%len(keys)])%len(letters)])
    return "".join(code)

def decode_msg(code, key):
    keys=[letters.index(c.upper()) for c in key]
    msg=[letters.index(c.upper()) for c in code]
    message=[]

    for i in range(len(code)):
        message.append(letters[(msg[i]-keys[i%len(keys)])%len(letters)])
    return "".join(message)

action=input("Would you like to (e)ncode or (d)ecode?")

if action is "encode" or action is 'e':
    message=input("Please enter a message to encode:\n")
    key=input("Please enter a key word or phrase:\n")
    code=encode_msg(message,key)
    print(code)
elif action is "decode" or action is 'd':
    message=input("Please enter a message to decode:\n")
    key=input("Please enter the keyword:\n")
    decode=decode_msg(message,key)
    print(decode)
